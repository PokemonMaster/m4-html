<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Activitat avaluable 2</title>
    <link rel="stylesheet" href="./ActAvaluable2.css">
</head>
<body>
    <header>
        <h1>Activitat de fitxers</h1>
    </header>
    <?php

        $linies = file($_FILES["fitxer"]["tmp_name"]);
        $fitxer = fopen('fitxers/fitxer','w');

        foreach ($linies as $nLinia => $linia){
          fwrite($fitxer, $linia);
        }
        $data = date("D/M/Y H:i:s");
        fwrite($fitxer, "Text pujat el: " . $data);
        fclose($fitxer);

        echo "El fitxer s'ha pujat correctament";
      ?>

      <figure>
         <img src="Thumbs-up.jpg" alt="thumbs up" height="200">
         <figcaption>Ben fet!!!</figcaption>
       </figure>

      <?php
        $file = file('fitxers/fitxer');
        echo "<pre>";
        print_r($file);
        echo "</pre>";

    ?>

    <footer>
        <p class="cr">Autor: Jaume Puig Soriano</p>
    </footer>
</body>
</html>
